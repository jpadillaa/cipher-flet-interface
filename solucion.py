import flet as ft
import cipher as cp

def main(page: ft.Page):
    page.title = "Basic Cipher/Decipher App"
    page.window_width = 640
    page.window_height = 480
    page.window_resizable = False
    page.padding = 48
    page.margin = 48

    def close_banner(e):
        page.banner.open = False
        page.update()

    page.banner = ft.Banner(
        bgcolor = ft.colors.RED,
        leading = ft.Icon(ft.icons.WARNING_AMBER_ROUNDED, color=ft.colors.AMBER, size = 40),
        content = ft.Text(
            "Oops, there were some errors while trying to encrypt or decrypt a message without the complete data (message and passkey)"
        ),
        actions = [
            ft.TextButton("Cancel", on_click = close_banner),
        ],
    )

    def dropdown_changed(e):        
        if dropdown_chipher.value == "Cipher" and  message.value != "" and passkey.value != "":
            answ = cp.cipher_endpoint("http://127.0.0.1:5000", message.value, passkey.value)
            encrypted_message.value = dict(answ)["ciphered"]
            encrypted_message.disabled = False 
            encrypted_message.visible = True 
            encrypted_message.label="Encrypted Message"
            encrypted_message.color = "PURPLE"
        
        elif dropdown_chipher.value == "Decipher" and message.value != "" and passkey.value != "":
            answ = cp.decipher_endpoint("http://127.0.0.1:5000", message.value, passkey.value)
            encrypted_message.value = dict(answ)["deciphered"]
            encrypted_message.disabled = False 
            encrypted_message.visible = True 
            encrypted_message.label="Decrypted Message"
            encrypted_message.color = "BLUE"

        else:
            page.banner.open = True
            page.update()
        
        message.value = "" 
        passkey.value = ""
        page.update()
        message.focus()
    
    message = ft.TextField(label="Message", autofocus = True)
    passkey = ft.TextField(label="Passkey", autofocus = False, color = "RED")
    encrypted_message = ft.TextField(label="Encrypted Message", autofocus = False, disabled = True, visible = False)
    
    dropdown_chipher = ft.Dropdown(
                    on_change = dropdown_changed,
                    label = "Action Selector",
                    width = 320,
                    options=[
                        ft.dropdown.Option("Cipher"),
                        ft.dropdown.Option("Decipher"),            
                    ],
                )
    columns = ft.Column()
    
    page.add(
        message,
        passkey,
        dropdown_chipher,     
        columns,
        encrypted_message
    )

ft.app(
    target = main,
    #view = ft.WEB_BROWSER,
)