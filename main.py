import flet as ft
import cipher as cp

def main(page: ft.Page):
    page.title = "Basic Cipher/Decipher App"
    page.window_width = 640
    page.window_height = 480
    page.window_resizable = False
    page.padding = 48
    page.margin = 48

    message = ft.TextField(label="Message", autofocus = True)
    passkey = ft.TextField(label="Passkey", color = "RED")

    page.add(
        message,
        passkey,
    )

ft.app(
    target = main,
    #view = ft.WEB_BROWSER,
)