import requests
import json

def cipher_endpoint(url, msg, passkey):
    """
    Realiza una solicitud POST al endpoint '/cipher' de un servidor dado para cifrar un mensaje.
    
    Args:
    - url (str): La URL base del servidor.
    - msg (str): El mensaje que se desea cifrar.
    - passkey (str): La clave para cifrar el mensaje.
    
    Returns:
    - dict: Un diccionario con la respuesta del servidor en formato JSON si la solicitud es exitosa.
    - str: En caso de error, devuelve un mensaje indicando el código de estado y el texto de respuesta.
    """
    endpoint = url + '/cipher'
    data = {"message": msg, "key": passkey}
    headers = {'Content-Type': 'application/json'}

    try:
        response = requests.post(endpoint, data=json.dumps(data), headers=headers)
        if response.status_code == 200:
            return response.json()  
        else:
            return f'Error: {response.status_code} - {response.text}'  
    except requests.exceptions.RequestException as e:
        return f'Error de conexión: {e}'  
    
def decipher_endpoint(url, msg, passkey):
    """
    Realiza una solicitud POST al endpoint '/decipher' de un servidor dado para descifrar un mensaje.
    
    Args:
    - url (str): La URL base del servidor.
    - msg (str): El mensaje cifrado que se desea descifrar.
    - passkey (str): La clave para descifrar el mensaje.
    
    Returns:
    - dict: Un diccionario con la respuesta del servidor en formato JSON si la solicitud es exitosa.
    - str: En caso de error, devuelve un mensaje indicando el código de estado y el texto de respuesta.
    """
    endpoint = url + '/decipher'
    data = {"message": msg, "key": passkey}
    headers = {'Content-Type': 'application/json'}

    try:
        response = requests.post(endpoint, data=json.dumps(data), headers=headers)
        if response.status_code == 200:
            return response.json()  
        else:
            return f'Error: {response.status_code} - {response.text}'  
    except requests.exceptions.RequestException as e:
        return f'Error de conexión: {e}'  
