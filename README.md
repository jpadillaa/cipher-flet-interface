# Cipher Flet Interface

Una interfaz web sencilla que utiliza Python y el framework Flet para consumir los endpoints de una API Rest. Es relevante mencionar que esta API no cuenta con autenticación, permitiendo que nuestra aplicación consuma directamente sus recursos. La aplicación en Flet se comunica con el backend en Flask a través de solicitudes HTTP.

Se consume una API REST desarrollada en Python y Flask. API básica que proporciona 2 endpoints, para cifrar y descifrar información utilizando el algoritmo RC4 utilizado en WEP. Se utiliza el API Rest del siguiente repositorio:
* [https://gitlab.com/jpadillaa/sasaki-python.git](https://gitlab.com/jpadillaa/sasaki-python.git)

## Los endpoints de la API
* Ruta Endpoint 1 [POST]: ```http://ip_servidor:5000/cipher```
* Ruta Endpoint 2 [POST]: ```http://ip_servidor:5000/decipher```
* Ambos Endpoints requieren un JSON con el siguiente formato:  
```{"message" : "Texto a Cifrar/Descifrar", "key": "llave para Cifrar/Descifrar"}```

## La aplicación web - Cipher Flet Interface
Este repositorio contiene una plantilla para seguir un tutorial paso a paso para construir esta interfaz con Flet. Es una base de código con algunas funciones desarrolladas y un esquema sencillo para enfocarnos completamente en el desarrollo de la interfaz.

El repositorio cuenta con los siguientes archivos:

* **cipher.py**: Contiene dos funciones que consumen los servicios del API Rest utilizando la librería requests de Python. Una función realiza una solicitud POST al endpoint /cipher para cifrar un mensaje y la otra función realiza una solicitud POST al endpoint /decipher para descifrar un mensaje.

* **main.py**: Es un esqueleto con algunos elementos en la función main para construir la interfaz con Flet.

* **solucion.py**: Es un archivo con la solución del tutorial (toda la interfaz desarrollada con Flet).

Realice la instalación de Flet ejecutando el comando a continuación:
* ```pip install flet```

## Link al tutorial 
* [https://jpadillaa.hashnode.dev/aplicacion-web-con-python-y-flet](https://jpadillaa.hashnode.dev/aplicacion-web-con-python-y-flet)